/* eslint-disable @typescript-eslint/camelcase */
import { ContextMessageUpdate, Markup } from 'telegraf';

export default async ({ reply }: ContextMessageUpdate) => {

  Markup.removeKeyboard();

  const keyboard = Markup.keyboard([
    ['/add'],
    ['/top', '/top_pair', '/top_single'],
    ['/delete']
  ]).forceReply(false).oneTime().resize().extra();

  return reply('controll panel', keyboard);
};
