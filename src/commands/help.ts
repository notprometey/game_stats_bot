import { ContextMessageUpdate } from 'telegraf';
import MESSAGE from '../message';

export default ({ reply }: ContextMessageUpdate) => reply(MESSAGE.help);
