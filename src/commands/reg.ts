import { ContextMessageUpdate } from 'telegraf';
import { getCustomRepository } from 'typeorm';
import { PlayerRepository } from '../db/repository/PlayerRapository';

export default async ({ message, reply, chat }: ContextMessageUpdate) => {
  const playerRepository = getCustomRepository(PlayerRepository);

  const name = message.text.replace('/reg ', '');

  try {
    const player = await playerRepository.addPlayer(chat.id, name);

    return reply(JSON.stringify(player));
  } catch (e) {
    return reply(e.message);
  }
};