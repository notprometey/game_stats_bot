import { ContextMessageUpdate } from 'telegraf';
import { getCustomRepository } from 'typeorm';
import { TeamRepository, Stats } from '../db/repository/TeamRapository';
import message from '../message';

const gifts = ['🥇', '🥈', '🥉'];

function getTopMessage(stats: Stats[]) {
  const messages: string[] = stats
    .sort((a, b) => b.percent - a.percent)
    .map(({ name, winCount, percent }, index: number) => {
      return `${index + 1} - ${name} ${gifts[index] || ''} [W${winCount} - ${percent}%]`;
    });

  return messages && messages.length ? messages.join('\n') : message.empty;
}

async function getTop(chatId: string | number, prepareStat: (stats: Stats[]) => Stats[]) {
  const teamRepository = getCustomRepository(TeamRepository);

  try {
    const stats: Stats[] = await teamRepository.getStats(chatId);
  
    return getTopMessage(
      prepareStat(stats)
    );
  } catch (e) {
    throw e;
  }
}

export const top = async ({ reply, chat }: ContextMessageUpdate) => {
  try {
    const message: string = await getTop(chat.id, (stats) => stats);
  
    return reply(message);
  } catch (e) {
    return reply(e.message);
  }
};

export const topSingle = async ({ reply, chat }: ContextMessageUpdate) => {
  try {
    const message: string = await getTop(chat.id, (stats) => stats.filter(s => s.playerCount === 1));
  
    return reply(message);
  } catch (e) {
    return reply(e.message);
  }
};

export const topPair = async ({ reply, chat }: ContextMessageUpdate) => {
  try {
    const message: string = await getTop(chat.id, (stats) => stats.filter(s => s.playerCount === 2));
  
    return reply(message);
  } catch (e) {
    return reply(e.message);
  }
};
