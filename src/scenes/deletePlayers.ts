import { BaseScene, Extra, Markup, Stage } from 'telegraf';
import { getCustomRepository } from 'typeorm';
import { PlayerRepository } from '../db/repository/PlayerRapository';
import message from '../message';

const NAME = 'delete';

const { leave } = Stage;

let selected: number[] = [];

const scene = new BaseScene(NAME);

async function getPlayerButton(id: number, chat: string | number) {
  const playerRepository = getCustomRepository(PlayerRepository);

  const index = selected.findIndex(item => item === id);

  if (index > -1) {
    selected.splice(index, 1);
  } else if (id !== -1) {
    selected.push(id);
  }

  try {
    const players = await playerRepository.getAll(chat);

    if (players.length === 0) {
      return null;
    }

    const inlineKeyboard = players.map(palyer => [Markup.callbackButton(`${palyer.name} ${selected.includes(palyer.id) ? '✅' : ''}`, `select:${palyer.id}`)]);

    inlineKeyboard.push([Markup.callbackButton('cancel', 'cancel')]);

    if (selected.length) {
      inlineKeyboard.push([Markup.callbackButton('Delete selected', 'delete')]);
    }

    const keyboard = Markup.inlineKeyboard(inlineKeyboard);

    return keyboard;
  } catch (e) {
    throw e;
  }
}

scene.enter(async ({ reply, chat }) => {
  try {
    const keyboard = await getPlayerButton(-1, chat.id);

    if (keyboard) {
      return reply('Select Players:', Extra.markup(keyboard));
    } else {
      return reply(message.emptyPlayer);
    }
  } catch (e) {
    return reply(e.message);
  }
});

scene.action('cancel', async ({ deleteMessage }) => {
  leave();
  deleteMessage();

  selected = [];
});

scene.action('delete', async ({ deleteMessage, reply }) => {
  leave();
  deleteMessage();

  const playerRepository = getCustomRepository(PlayerRepository);

  const ids = [...selected];

  selected = [];

  return playerRepository.deleteByIds(ids)
  .then(() => reply('Deleted successfully'))
  .catch((e) => reply(e.message));
});

scene.action(/select:/, async ({ editMessageReplyMarkup, reply, chat, match }) => {
  const id = Number(match.input.split(':').pop());

  try {
    const keyboard =await getPlayerButton(id, chat.id);

    return editMessageReplyMarkup(keyboard).catch(console.log);
  } catch (e) {
    return reply(e.message);
  }
});

export default { scene, name: NAME };