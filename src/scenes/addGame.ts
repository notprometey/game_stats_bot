import { BaseScene, Extra, Markup, Stage } from 'telegraf';
import { getCustomRepository } from 'typeorm';
import { PlayerRepository } from '../db/repository/PlayerRapository';
import { TeamRepository } from '../db/repository/TeamRapository';
import { Team } from '../db/entity/Team';
import { TeamToGameRepository } from '../db/repository/TeamToGameRapository';
import { GameRepository } from '../db/repository/GameRapository';
import message from '../message';

const NAME = 'add';

const { leave } = Stage;

let selected: number[] = [];
let teams: Team[] = [];
let counts: number[] = [];

const scene = new BaseScene(NAME);

async function getCountButton(team?: number, count?: number) {
  const inlineKeyboard = [];

  counts[team - 1] = count;

  for(let i = 1; i < 16; i += 1) {
    inlineKeyboard.push([Markup.callbackButton(`${i} ${counts[0] === i ? '✅' : ''}`, `team:1:${i}`), Markup.callbackButton(`${i} ${counts[1] === i ? '✅' : ''}`, `team:2:${i}`)]);
  }

  inlineKeyboard.push([Markup.callbackButton('cancel', 'cancel')]);

  if(counts.length === 2 && counts[0] !== counts[1]) {
    inlineKeyboard.push([Markup.callbackButton('Add Game', 'add')]);
  }

  return Markup.inlineKeyboard(inlineKeyboard);
}

async function getPlayerButton(id: number, chat: string | number) {
  const playerRepository = getCustomRepository(PlayerRepository);

  if (selected.length < 4) {
    const index = selected.findIndex(item => item === id);

    if (index > -1) {
      selected.splice(index, 1);
    } else if (id !== -1) {
      selected.push(id);
    }
  }

  function getTeamName(list: number[], id: number): string {
    const index = list.findIndex(item => item === id);
    const teamCount = Math.ceil(list.length / 2);

    if(index !== -1) {
      if(teamCount > index) {
        return 'team 1';
      } else {
        return 'team 2';
      }
    }

    return '';
  }

  try {
    const players = await playerRepository.getAll(chat);

    if (players.length === 0) {
      return null;
    }

    const inlineKeyboard = players.map(player => [Markup.callbackButton(`${player.name} ${selected.includes(player.id) ? `✅ ${getTeamName(selected, player.id)}` : ''}`, `select:${player.id}`)]);

    inlineKeyboard.push([Markup.callbackButton('cancel', 'cancel')]);

    if (selected.length === 2 || selected.length === 4) {
      inlineKeyboard.push([Markup.callbackButton('Create geme', 'create')]);
    }

    const keyboard = Markup.inlineKeyboard(inlineKeyboard);

    return keyboard;
  } catch (e) {
    throw e;
  }
}

scene.enter(async ({ reply, chat }) => {
  try {
    const keyboard = await getPlayerButton(-1, chat.id);

    if(keyboard) {
      return reply('Select Players', Extra.markup(keyboard));
    } else {
      return reply(message.emptyPlayer);
    }
  } catch (e) {
    return reply(e.message);
  }
});

scene.action('cancel', async ({ deleteMessage }) => {
  leave();
  deleteMessage();

  selected = [];
  teams = [];
  counts = [];
});

scene.action('create', async ({ editMessageReplyMarkup, editMessageText, chat }) => {
  const ids = [...selected];

  selected = [];

  const team1 = ids.slice(0, (ids.length / 2));
  const team2 = ids.slice((ids.length / 2));

  const teamRepository = getCustomRepository(TeamRepository);
  const playerRepository = getCustomRepository(PlayerRepository);

  const playerTeam1 = await playerRepository.getByIds(chat.id, team1);
  const playerTeam2 = await playerRepository.getByIds(chat.id, team2);

  teams.push(await teamRepository.addTeam(chat.id, playerTeam1));
  teams.push(await teamRepository.addTeam(chat.id, playerTeam2));

  const keyboard = await getCountButton();

  await editMessageText(`Fill game amount for ${playerTeam1.map(p => p.name).join(' & ')} vs ${playerTeam2.map(p => p.name).join(' & ')}`);
  return editMessageReplyMarkup(keyboard);
});

scene.action('add', async ({ deleteMessage, reply, chat }) => {
  const gameRepository = getCustomRepository(GameRepository);
  const teamToGameRepository = getCustomRepository(TeamToGameRepository);

  const game = await gameRepository.addGame(chat.id);

  await teamToGameRepository.addTeamToGame(game, teams[0], counts[0], counts[0] > counts[1] ? 1 : 0);
  await teamToGameRepository.addTeamToGame(game, teams[1], counts[1], counts[1] > counts[0] ? 1 : 0);

  leave();
  await deleteMessage();
  await reply(`Success: ${teams[0].players.map(p => p.name).join(' & ')} vs ${teams[1].players.map(p => p.name).join(' & ')} - ${counts[0]}:${counts[1]}`);

  selected = [];
  teams = [];
  counts = [];
});

scene.action(/select:/, async ({ editMessageReplyMarkup, reply, chat, match }) => {
  const id = Number(match.input.split(':').pop());

  try {
    const keyboard = await getPlayerButton(id, chat.id);

    return editMessageReplyMarkup(keyboard).catch(console.log);
  } catch (e) {
    return reply(e.message);
  }
});

scene.action(/team:(\d):(\d+)/, async ({ editMessageReplyMarkup, reply, match }) => {
  const [, team, count] = match;

  try {
    const keyboard = await getCountButton(Number(team), Number(count));

    return editMessageReplyMarkup(keyboard).catch(console.log);
  } catch (e) {
    return reply(e.message);
  }
});

export default { scene, name: NAME };
