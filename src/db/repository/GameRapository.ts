import { EntityRepository, Repository } from 'typeorm';
import { Game } from '../entity/Game';

@EntityRepository(Game)
export class GameRepository extends Repository<Game> {

  async addGame(chat: string | number) {
    const game = this.create();

    game.chat = chat;

    return this.save(game);
  }

}