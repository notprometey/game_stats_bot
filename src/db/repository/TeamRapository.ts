import { EntityRepository, Repository } from 'typeorm';
import { Team } from '../entity/Team';
import { Player } from '../entity/Player';

export const WIN_STAUS = 1;
export const LOSS_STAUS = 0;

export interface Stats {
  name: string;
  playerCount: number;
  winCount: number;
  lossCount: number;
  count: number;
  percent: number;
}

@EntityRepository(Team)
export class TeamRepository extends Repository<Team> {

  async addTeam(chat: string | number, players: Player[]) {

    const teams = await this.find({
      join: {
        alias: 'team',
        leftJoinAndSelect: {
          players: 'team.players',
        }
      },
      where: {
        chat: chat
      }
    });

    function compare(a: Array<any>, b: Array<any>): boolean {
      return a.length === b.length && a.every(e => b.includes(e));
    }

    let team = teams.find(team => compare(team.players.map(p => p.id), players.map(p => p.id)));

    if (team) {
      return team;
    }

    team = this.create();

    team.players = players;
    team.chat = chat;

    return this.save(team);
  }

  async getStats(chat: string | number): Promise<Stats[]> {
    const teams = await this.find({
      join: {
        alias: 'team',
        leftJoinAndSelect: {
          teamToGame: 'team.teamToGame',
          players: 'team.players'
        }
      },
      where: {
        chat: chat
      }
    });

    return teams
      .filter(team => team.players.length)
      .map(team => {
        return {
          name: team.players.length ? team.players.map(p => p.name).join(' & ') : `team#${team.id}`,
          playerCount: team.players.length,
          winCount: team.teamToGame.filter(game => game.status === WIN_STAUS).length,
          lossCount: team.teamToGame.filter(game => game.status === LOSS_STAUS).length,
          count: team.teamToGame.length,
          get percent() {
            return this.count ? (this.winCount / this.count) * 100 : 0;
          }
        };
    });
  }

}