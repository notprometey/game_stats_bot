import { EntityRepository, Repository } from 'typeorm';
import { Game } from '../entity/Game';
import { TeamToGame } from '../entity/TeamToGame';
import { Team } from '../entity/Team';

@EntityRepository(TeamToGame)
export class TeamToGameRepository extends Repository<TeamToGame> {

  async addTeamToGame(game: Game, team: Team, value: number, status: number) {
    const teamToGame = this.create();

    teamToGame.game = game;
    teamToGame.team = team;

    teamToGame.value = value;
    teamToGame.status = status;

    return this.save(teamToGame);
  }

}