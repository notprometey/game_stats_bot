import { EntityRepository, Repository } from 'typeorm';
import { Player } from '../entity/Player';

@EntityRepository(Player)
export class PlayerRepository extends Repository<Player> {

  addPlayer(chat: string | number, name: string) {
    const player = this.create();

    player.name = name;
    player.chat = chat;

    return this.save(player);
  }

  getAll(chat: string | number) {
    return this.find({ where: { chat } });
  }

  getByIds(chat: string | number, ids: number[]) {
    return this.findByIds(ids, { where: { chat } });
  }

  deleteByIds(ids: Array<number>) {
    return this.delete(ids);
  }

}