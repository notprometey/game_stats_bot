import {Entity, PrimaryGeneratedColumn, OneToMany, JoinTable, ManyToMany, Column} from 'typeorm';
import { TeamToGame } from './TeamToGame';
import { Player } from './Player';

@Entity()
export class Team {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('text')
  chat: string | number;

  @ManyToMany(type => Player)
  @JoinTable()
  players: Player[];

  @OneToMany(type => TeamToGame, teamToGame => teamToGame.team)
  teamToGame!: TeamToGame[];
}
