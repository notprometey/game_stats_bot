import { Entity, Column, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Game } from './Game';
import { Team } from './Team';

@Entity()
export class TeamToGame {
    @PrimaryGeneratedColumn()
    public postToCategoryId!: number;

    @Column()
    public gameId!: number;
    
    @Column()
    public teamId!: number;

    @Column()
    public value!: number;

    @Column()
    public status!: number;

    @ManyToOne(type => Game, post => post.teamToGame)
    public game!: Game;

    @ManyToOne(type => Team, category => category.teamToGame)
    public team!: Team;
}