import { Entity, PrimaryGeneratedColumn, OneToMany, Column } from 'typeorm';
import { TeamToGame } from './TeamToGame';

@Entity()
export class Game {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('text')
  chat: string | number;
  
  @OneToMany(type => TeamToGame, teamToGame => teamToGame.game)
  teamToGame!: TeamToGame[];
}