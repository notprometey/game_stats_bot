import {MigrationInterface, QueryRunner} from 'typeorm';

export class Typo1580575403060 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('CREATE TABLE "team_players_player" ("teamId" integer NOT NULL, "playerId" integer NOT NULL, PRIMARY KEY ("teamId", "playerId"))', undefined);
        await queryRunner.query('CREATE INDEX "IDX_03530e45522b82c6ae46d825dd" ON "team_players_player" ("teamId") ', undefined);
        await queryRunner.query('CREATE INDEX "IDX_a5a5ca467eb43bf810ce32a119" ON "team_players_player" ("playerId") ', undefined);
        await queryRunner.query('DROP INDEX "IDX_03530e45522b82c6ae46d825dd"', undefined);
        await queryRunner.query('DROP INDEX "IDX_a5a5ca467eb43bf810ce32a119"', undefined);
        await queryRunner.query('CREATE TABLE "temporary_team_players_player" ("teamId" integer NOT NULL, "playerId" integer NOT NULL, CONSTRAINT "FK_03530e45522b82c6ae46d825dd1" FOREIGN KEY ("teamId") REFERENCES "team" ("id") ON DELETE CASCADE ON UPDATE NO ACTION, CONSTRAINT "FK_a5a5ca467eb43bf810ce32a119d" FOREIGN KEY ("playerId") REFERENCES "player" ("id") ON DELETE CASCADE ON UPDATE NO ACTION, PRIMARY KEY ("teamId", "playerId"))', undefined);
        await queryRunner.query('INSERT INTO "temporary_team_players_player"("teamId", "playerId") SELECT "teamId", "playerId" FROM "team_players_player"', undefined);
        await queryRunner.query('DROP TABLE "team_players_player"', undefined);
        await queryRunner.query('ALTER TABLE "temporary_team_players_player" RENAME TO "team_players_player"', undefined);
        await queryRunner.query('CREATE INDEX "IDX_03530e45522b82c6ae46d825dd" ON "team_players_player" ("teamId") ', undefined);
        await queryRunner.query('CREATE INDEX "IDX_a5a5ca467eb43bf810ce32a119" ON "team_players_player" ("playerId") ', undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('DROP INDEX "IDX_a5a5ca467eb43bf810ce32a119"', undefined);
        await queryRunner.query('DROP INDEX "IDX_03530e45522b82c6ae46d825dd"', undefined);
        await queryRunner.query('ALTER TABLE "team_players_player" RENAME TO "temporary_team_players_player"', undefined);
        await queryRunner.query('CREATE TABLE "team_players_player" ("teamId" integer NOT NULL, "playerId" integer NOT NULL, PRIMARY KEY ("teamId", "playerId"))', undefined);
        await queryRunner.query('INSERT INTO "team_players_player"("teamId", "playerId") SELECT "teamId", "playerId" FROM "temporary_team_players_player"', undefined);
        await queryRunner.query('DROP TABLE "temporary_team_players_player"', undefined);
        await queryRunner.query('CREATE INDEX "IDX_a5a5ca467eb43bf810ce32a119" ON "team_players_player" ("playerId") ', undefined);
        await queryRunner.query('CREATE INDEX "IDX_03530e45522b82c6ae46d825dd" ON "team_players_player" ("teamId") ', undefined);
        await queryRunner.query('DROP INDEX "IDX_a5a5ca467eb43bf810ce32a119"', undefined);
        await queryRunner.query('DROP INDEX "IDX_03530e45522b82c6ae46d825dd"', undefined);
        await queryRunner.query('DROP TABLE "team_players_player"', undefined);
    }

}
