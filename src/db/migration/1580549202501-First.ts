import {MigrationInterface, QueryRunner} from 'typeorm';

export class First1580549202501 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('CREATE TABLE "player" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar NOT NULL, "chat" text NOT NULL, CONSTRAINT "UQ_7baa5220210c74f8db27c06f8b4" UNIQUE ("name"))', undefined);
        await queryRunner.query('CREATE TABLE "team" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "chat" text NOT NULL)', undefined);
        await queryRunner.query('CREATE TABLE "team_to_game" ("postToCategoryId" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "gameId" integer NOT NULL, "teamId" integer NOT NULL, "value" integer NOT NULL, "status" integer NOT NULL)', undefined);
        await queryRunner.query('CREATE TABLE "game" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "chat" text NOT NULL)', undefined);
        await queryRunner.query('CREATE TABLE "team_palyers_player" ("teamId" integer NOT NULL, "playerId" integer NOT NULL, PRIMARY KEY ("teamId", "playerId"))', undefined);
        await queryRunner.query('CREATE INDEX "IDX_94d8907eb596153e421c70d792" ON "team_palyers_player" ("teamId") ', undefined);
        await queryRunner.query('CREATE INDEX "IDX_fd0eb6e9e4f1246c5f0f59d815" ON "team_palyers_player" ("playerId") ', undefined);
        await queryRunner.query('CREATE TABLE "temporary_team_to_game" ("postToCategoryId" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "gameId" integer NOT NULL, "teamId" integer NOT NULL, "value" integer NOT NULL, "status" integer NOT NULL, CONSTRAINT "FK_8bf0bcf6adc20e9b8ef06041a9c" FOREIGN KEY ("gameId") REFERENCES "game" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT "FK_ad0db3dcd48a83ca50f2a343f66" FOREIGN KEY ("teamId") REFERENCES "team" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION)', undefined);
        await queryRunner.query('INSERT INTO "temporary_team_to_game"("postToCategoryId", "gameId", "teamId", "value", "status") SELECT "postToCategoryId", "gameId", "teamId", "value", "status" FROM "team_to_game"', undefined);
        await queryRunner.query('DROP TABLE "team_to_game"', undefined);
        await queryRunner.query('ALTER TABLE "temporary_team_to_game" RENAME TO "team_to_game"', undefined);
        await queryRunner.query('DROP INDEX "IDX_94d8907eb596153e421c70d792"', undefined);
        await queryRunner.query('DROP INDEX "IDX_fd0eb6e9e4f1246c5f0f59d815"', undefined);
        await queryRunner.query('CREATE TABLE "temporary_team_palyers_player" ("teamId" integer NOT NULL, "playerId" integer NOT NULL, CONSTRAINT "FK_94d8907eb596153e421c70d792c" FOREIGN KEY ("teamId") REFERENCES "team" ("id") ON DELETE CASCADE ON UPDATE NO ACTION, CONSTRAINT "FK_fd0eb6e9e4f1246c5f0f59d8158" FOREIGN KEY ("playerId") REFERENCES "player" ("id") ON DELETE CASCADE ON UPDATE NO ACTION, PRIMARY KEY ("teamId", "playerId"))', undefined);
        await queryRunner.query('INSERT INTO "temporary_team_palyers_player"("teamId", "playerId") SELECT "teamId", "playerId" FROM "team_palyers_player"', undefined);
        await queryRunner.query('DROP TABLE "team_palyers_player"', undefined);
        await queryRunner.query('ALTER TABLE "temporary_team_palyers_player" RENAME TO "team_palyers_player"', undefined);
        await queryRunner.query('CREATE INDEX "IDX_94d8907eb596153e421c70d792" ON "team_palyers_player" ("teamId") ', undefined);
        await queryRunner.query('CREATE INDEX "IDX_fd0eb6e9e4f1246c5f0f59d815" ON "team_palyers_player" ("playerId") ', undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('DROP INDEX "IDX_fd0eb6e9e4f1246c5f0f59d815"', undefined);
        await queryRunner.query('DROP INDEX "IDX_94d8907eb596153e421c70d792"', undefined);
        await queryRunner.query('ALTER TABLE "team_palyers_player" RENAME TO "temporary_team_palyers_player"', undefined);
        await queryRunner.query('CREATE TABLE "team_palyers_player" ("teamId" integer NOT NULL, "playerId" integer NOT NULL, PRIMARY KEY ("teamId", "playerId"))', undefined);
        await queryRunner.query('INSERT INTO "team_palyers_player"("teamId", "playerId") SELECT "teamId", "playerId" FROM "temporary_team_palyers_player"', undefined);
        await queryRunner.query('DROP TABLE "temporary_team_palyers_player"', undefined);
        await queryRunner.query('CREATE INDEX "IDX_fd0eb6e9e4f1246c5f0f59d815" ON "team_palyers_player" ("playerId") ', undefined);
        await queryRunner.query('CREATE INDEX "IDX_94d8907eb596153e421c70d792" ON "team_palyers_player" ("teamId") ', undefined);
        await queryRunner.query('ALTER TABLE "team_to_game" RENAME TO "temporary_team_to_game"', undefined);
        await queryRunner.query('CREATE TABLE "team_to_game" ("postToCategoryId" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "gameId" integer NOT NULL, "teamId" integer NOT NULL, "value" integer NOT NULL, "status" integer NOT NULL)', undefined);
        await queryRunner.query('INSERT INTO "team_to_game"("postToCategoryId", "gameId", "teamId", "value", "status") SELECT "postToCategoryId", "gameId", "teamId", "value", "status" FROM "temporary_team_to_game"', undefined);
        await queryRunner.query('DROP TABLE "temporary_team_to_game"', undefined);
        await queryRunner.query('DROP INDEX "IDX_fd0eb6e9e4f1246c5f0f59d815"', undefined);
        await queryRunner.query('DROP INDEX "IDX_94d8907eb596153e421c70d792"', undefined);
        await queryRunner.query('DROP TABLE "team_palyers_player"', undefined);
        await queryRunner.query('DROP TABLE "game"', undefined);
        await queryRunner.query('DROP TABLE "team_to_game"', undefined);
        await queryRunner.query('DROP TABLE "team"', undefined);
        await queryRunner.query('DROP TABLE "player"', undefined);
    }

}
