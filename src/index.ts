import 'reflect-metadata';

require('dotenv').config();

import Telegraf, { session, SceneContextMessageUpdate, Stage } from 'telegraf';

import start from './commands/start';
import help from './commands/help';
import reg from './commands/reg';
import { createConnection } from 'typeorm';
import deletePlayers from './scenes/deletePlayers';
import addGame from './scenes/addGame';
import { top, topPair, topSingle } from './commands/top';

const bot = new Telegraf<SceneContextMessageUpdate>(process.env.TELEGRAM_TOKEN || '');

const stage = new Stage([deletePlayers.scene, addGame.scene]);

bot.use(session());
bot.use(stage.middleware());

createConnection();

bot.start(start);
bot.help(help);

bot.command('reg', reg);
bot.command('top', top);
bot.command('top_pair', topPair);
bot.command('top_single', topSingle);
bot.command('delete', (ctx) => ctx.scene.enter(deletePlayers.name));
bot.command('add', (ctx) => ctx.scene.enter(addGame.name));

bot.startPolling();

