export default {
  invalidCommand: 'Sad... \nTry something like: \n/game @mention @mention [11:12] \n/game @mention @mention @mention @mention [11:12]',
  invalidValue: 'Someone must win',
  invalidPlayer: 'They have only one racket',
  empty: 'Empty top 😢',
  emptyPlayer: 'Empty player list 😢. Using /reg <name> command',
  start: 'Born to Win',
  help: '/help - list of commands \n/add - game registration \n/reg - player registration \n/delete - delete players \n/top - top of all games \n/top_single - top of single games \n/top_pair - top of pair games',
  congratulations: 'Congratulations',
  unexpectedError: 'Something went wrong'
};
